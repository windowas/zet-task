<!doctype html>
<html lang="{{ app()->getLocale() }}">

   @include('includes.head')
    <body>
        <div class="container">
                <div class="jumbotron">
                    <h1 class="display-1 font-weight-bold text-primary">The PicPic Site</h1>
                </div>
                <div class="content">
                    @yield('content')
                </div>
            </div>
    </body>
</html>