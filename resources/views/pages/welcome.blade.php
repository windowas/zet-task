
@extends('layouts.base')
@section('content')

    <form action="{{ url('welcome') }}" class="form-image-upload" method="POST" enctype="multipart/form-data">

        {!! csrf_field() !!}

        @include('includes.error_message')

        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
        @endif


        <div class="row">
            <div class="col-md-5">
                <strong class="display-4">Title:</strong>
                <input type="text" name="title" class="form-control" placeholder="Title">
            </div>
            <div class="col-md-5">
                <strong  class="display-4">Image:</strong>
                <input type="file" name="image" class="form-control" accept=".png,.gif,.jpg,.jpeg">
            </div>
            <div class="col-md-2">
                <br/>
                <button type="submit" class="btn btn-success btn-lg">Upload</button>
            </div>
        </div>

    </form>


    <div class="row">
        <div class='list-group gallery'>


            @if($images->count())
                @foreach($images as $image)
                    <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                        <a class="thumbnail fancybox" rel="ligthbox" href="{{ asset('/images/' . $image->image) }}">
                            <img class="img-responsive" alt="" src="{{ asset('/images/' . $image->image) }}" />
                            <div class='text-center'>
                                <small class='text-muted'>{{ $image->title }}</small>
                            </div> <!-- text-center / end -->
                        </a>
                        <form action="{{ url('welcome',$image->id) }}" method="POST">
                            <input type="hidden" name="_method" value="delete">
                            {!! csrf_field() !!}
                            <button type="submit" class="close-icon btn btn-danger"><i class="glyphicon glyphicon-remove"></i></button>
                        </form>
                    </div>
                @endforeach
            @endif


        </div>
    </div>
    <div class="row pagination">
        {{ $images->links() }}
    </div>

@endsection
